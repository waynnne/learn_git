# -*- coding: utf-8 -*-
# Author : Li Wei
# Date   : 2020/4/9 17:41

import random

class RandomUtil():

    def __init__(self):
        pass

    def random_number(self, num):
        """
        随机生成数字
        :param num:
        :return:
        """
        return random.randint(1, num)

    def random_string(self, num):
        """
        随机生成字符
        :param num:
        :return:
        """
        random_str = ''
        base_str = 'ABCDEFGHIGKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789'
        length = len(base_str) - 1
        for i in range(num):
            random_str += base_str[random.randint(0, length)]
        return random_str