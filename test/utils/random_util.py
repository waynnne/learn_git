# -*- coding: utf-8 -*-
# Author : Li Wei
# Date   : 2020/4/9 17:48
from main.utils.random_util import RandomUtil


def random_number_test():
    """
    测试random_number
    :return:
    """
    for i in range(10):
        print(RandomUtil().random_number(10))

def random_string_test():
    """
    测试random_string
    :return:
    """
    for i in range(10):
        print(RandomUtil().random_string(10))


if __name__ == '__main__':
    random_number_test()
    random_string_test()